FROM openjdk:latest
EXPOSE 8990
ADD target/pet-service-0.0.1.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]