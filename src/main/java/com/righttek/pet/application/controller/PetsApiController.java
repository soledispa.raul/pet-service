package com.righttek.pet.application.controller;

import com.righttek.pet.application.contracts.PetsApi;
import com.righttek.pet.application.dto.ApiResponseMessage;
import com.righttek.pet.application.dto.ModelApiResponse;
import com.righttek.pet.application.dto.Pet;
import com.righttek.pet.application.dto.PetListResponseMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.righttek.pet.application.services.PetService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-09T13:30:09.023-05:00")

@Controller
public class PetsApiController implements PetsApi {


    @Autowired
    private PetService petService;

    private static final Logger log = LoggerFactory.getLogger(PetsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public PetsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<ApiResponseMessage> addPet(@ApiParam(value = "Pet object that needs to be added to the store" ,required=true )  @Valid @RequestBody Pet pet) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<ApiResponseMessage>(petService.addPet(pet), HttpStatus.OK);
        }

        return new ResponseEntity<ApiResponseMessage>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<PetListResponseMessage> getPets() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<PetListResponseMessage>(petService.getPets(), HttpStatus.OK);
        }

        return new ResponseEntity<PetListResponseMessage>(HttpStatus.NOT_IMPLEMENTED);
    }

}
