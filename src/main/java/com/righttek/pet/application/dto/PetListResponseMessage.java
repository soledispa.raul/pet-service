package com.righttek.pet.application.dto;

import java.io.Serializable;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * PetListResponseMessage
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-09T13:30:09.023-05:00")

public class PetListResponseMessage implements Serializable {
  @JsonProperty("response")
  private ApiResponseMessage response = null;

  @JsonProperty("data")
  @Valid
  private List<Pet> data = null;


  /**
   * Get response
   * @return response
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ApiResponseMessage getResponse() {
    return response;
  }

  public void setResponse(ApiResponseMessage response) {
    this.response = response;
  }

  public PetListResponseMessage data(List<Pet> data) {
    this.data = data;
    return this;
  }

  public PetListResponseMessage addDataItem(Pet dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<Pet>();
    }
    this.data.add(dataItem);
    return this;
  }

  /**
   * Get data
   * @return data
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Pet> getData() {
    return data;
  }

  public void setData(List<Pet> data) {
    this.data = data;
  }

  public PetListResponseMessage(ApiResponseMessage response, List<Pet> data) {
    this.response = response;
    this.data = data;
  }
}

