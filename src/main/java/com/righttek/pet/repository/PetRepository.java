package com.righttek.pet.repository;
import com.righttek.pet.application.dto.Pet;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;

@Repository
public class PetRepository {

    public PetRepository(){}

    private ArrayList<Pet> pets = new ArrayList<>();


    public void addPet(Pet pet){

        pets.add(pet);

    }


    public ArrayList<Pet> getPets(){
        return pets;
    }


}
